package Learn_array;

public class Array_pratice {
public static void main(String[] args) {
	Array_pratice a1=new Array_pratice();
	a1.even();
	//a1.score();
	//a1.day();
	//a1.num();
	//a1.num1();
	//a1.count();
	//a1.total();
	//a1.each();
	//a1.reverse();
	//a1.Break();
	//a1.Continue();
	
}


private void Continue() {
	int [] num= {10,20,30,40,50};
	for(int i=0;i<num.length;i++) {
		if(i==2) {
			continue;
		}
		System.out.println(num[i]);
	}
	
}


private void Break() {
	int [] num= {10,20,30,40,50};
	for(int i=0;i<num.length;i++) {
		if(i==1) {
			break;
		}
		System.out.println(num[i]);
	}
}


private void reverse() {
	int [] num= {10,20,30,40,50};
	for(int i=4;i>=0;i--) {
		System.out.println(num[i]);
	}
	
}


private void each() {
	int [] marks= {88,75,90,89,76};
	for (int i : marks) {
		System.out.println(i);
	}
	
}


private void total() {
	int [] sum= {88,75,90,89,76};
	int amount=0;
	for(int i=0;i<sum.length;i++) {
		amount=amount+sum[i];
	}
	System.out.println(amount);
}


private void count() {
	int [] mark= {98,34,56,78,27};
	int count=0;
	for(int i=0;i<mark.length;i++) {
		if(mark[i]<35) {
			count++;
		}
	}
	System.out.println(count);
}


private void num1() {
	int [] single= {1,2,3,4,5};
	for(int i=0;i<single.length;i++) {
		for(int j=0;j< single.length;j++) {
			System.out.print(single[i]);
		}
		System.out.println();
	}
	
}

private void num() {
	int [] single= {1,2,3,4,5};
	for(int i=0;i<single.length;i++) {
		System.out.print(single[i]+" ");
	}
	
}

private void day() {
	int [] week= {1,2,3,4,5,6,7};
	for(int i=0;i<week.length;i++) {
		if(week[i]==1) {
			System.out.println("monday");
		}
		else if(week[i]==2) {
			System.out.println("tuesday");
		}
		else if(week[i]==3) {
			System.out.println("wednesday");
		}
		else if(week[i]==4) {
			System.out.println("thusday");
		}
		else if(week[i]==5) {
			System.out.println("friday");
		}
		else if(week[i]==6) {
			System.out.println("saturday");
		}
		else if(week[i]==7) {
			System.out.println("sunday");
		}
		else {
			System.out.println("no");
		}
		
		
	}
	
}

private void score() {
	int [] mark= {70,60,20,80,10};
	for(int i=0;i<mark.length;i++) {
		if(mark[i]>50) {
			System.out.println("pass");
		}
		else if(mark[i]<50) {
			System.out.println("fail");
		}
	}
	
}

private void even() {
	int [] num= {1,2,3,4,5,6,7,8,9,10};
	for(int i=0;i<num.length;i++) {
		if(num[i]%2!=0) {
			System.out.println(num[i]);
		}
	}
	
}
}
