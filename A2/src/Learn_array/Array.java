package Learn_array;

public class Array {
	public static void main(String[] args) {
		Array a1 = new Array();
		 a1.arraywhile();
		 a1.arrayforloop();
		//a1.length();

	}

	private void length() {

		int[] marks = new int[5];
		marks[0] = 23;
		marks[1] = 24;
		marks[2] = 25;
		marks[3] = 26;
		marks[4] = 27;
		for (int index = 0; index < marks.length; index++) {
			if (marks[index] % 2 == 0) { 
				// print add numbers use !=0.
				//print even numbers use ==0.
				System.out.println(marks[index]);
			}
		}

	}

	private void arrayforloop() {
		int[] marks = { 90, 97, 80, 88, 100 };

		for (int index = 0; index < 5; index++) { /// for loop
			System.out.print(marks[index]+" ");
		}
	}

	private void arraywhile() {
		int[] marks = { 90, 97, 80, 88, 100 };
		int index = 0; // while loop
		while (index < 5) {
			System.out.print(marks[index]+" ");
			index++;
		}

	}
}
