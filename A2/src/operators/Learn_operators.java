package operators;

public class Learn_operators {
      public static void main(String[] args) {
		int no=10;
		//post unary operator
		System.out.println(no++);
		System.out.println(no);
		
		//post unary operator
		System.out.println(no--);
		System.out.println(no);
		
		//pre unary operator
		System.out.println(++no);
		
		//pre unary operator
		System.out.println(--no);
		
		
		//OR gate
		int no1=20;
		int no2=30;
		System.out.println(no1<no2 || ++no2<no1);
		System.out.println(no2);
		
		
		//AND gate                                                                                                 gate
		int no3=20;
		int no4=30;
		System.out.println(no3<no4 && ++no4>no3);
		System.out.println(no4);
		
		
		int day = 7;
		int marks = 100;
		
		//ternary operator
		String result = marks > 50 ? "pas" : "fail";
		System.out.println(result);

		if (marks >= 50) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}
		
	}
}
