package oops_pratice;

public abstract class FactoryDemo extends Smartphone {
	boolean isOriginalPiece = false;
	int price = 0;

	public void browse() {
		System.out.println("Factory Demo browsing");
	}

	void call(int seconds) {

	}

	void sendMessage() {

	}

	void receiveCall() {

	}

	abstract void verifyFingerPrint();

	abstract void providePattern();
}
