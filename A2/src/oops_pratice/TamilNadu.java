package oops_pratice;

public class TamilNadu extends SouthIndia {
	static String capital = "Chennai";

	public static void main(String[] args) {
		System.out.println(India.capital);
		System.out.println(TamilNadu.capital);
		SouthIndia si = new TamilNadu();
		si.cultivate();
		si.livingStyle();
	}

	public void cultivate() {
		System.out.println("Rice and Sugar cane cultivation");
	}

	public void livingStyle() {
		System.out.println("Above Average development");
	}

	void speakLanguage() {
		System.out.println("speak method");
	}

	void eat() {
		System.out.println("eat method");
	}

	void dress() {
		System.out.println("dress method");
	}
}
