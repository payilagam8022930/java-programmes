package learn_file_io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reader {
	public static void main(String[] args) {
		File note = new File("/home/vigneshwaran/Videos/new file");

		try {
			FileReader reader = new FileReader(note);
			int i = reader.read();
			while (i != -1) {
				System.out.print((char)i);
				i = reader.read();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}

	}
}
