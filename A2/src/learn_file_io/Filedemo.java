package learn_file_io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class Filedemo {

	public static void main(String[] args) {
		//make one directory /vicky.
		// File ff = new File("/home/vigneshwaran/Videos/vicky"); 
		// System.out.println(ff.mkdir());
		
		//make two directory /amma/appa.
		// File ff = new File("/home/vigneshwaran/Videos/amma/appa"); 
		// System.out.println(ff.mkdirs());
		
		// create a file in directory.
		File ff = new File("/home/vigneshwaran/Videos/amma/appa/house123.txt"); 
		try {
			//create file in directory function.
			ff.createNewFile();    
			
			//write a letters or anything in file use this object.
			// (\n) used to print the letter at nextline.
			FileWriter pen=new FileWriter(ff,true);
//			pen.append("\nfamily");
//			pen.append("\namma,appa"); 
//			pen.close();
			
			// advance model for filewriter(BufferedWriter).
			BufferedWriter bw=new  BufferedWriter(pen);
			bw.newLine();
			bw.append("vicky");
			bw.newLine();
			bw.append("amma");
			bw.newLine();
			bw.append("appa");
			bw.newLine();
			bw.flush();
			bw.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	//	System.out.println(ff.canRead());   //read the file
	//	System.out.println(ff.canWrite());  //write the file
	//	System.out.println(ff.delete());   // delete the file
		
		
	}

}
