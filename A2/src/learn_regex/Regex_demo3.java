package learn_regex;

import java.util.regex.*;

public class Regex_demo3 {
	public static void main(String[] args) {
		String password = "Chennai is the capital city";
		
		//Pattern patternObj = Pattern.compile("\\s");  //s calculate space.
		
		//Pattern patternObj = Pattern.compile("\\S");   //S skip the space.
		
	    //Pattern patternObj = Pattern.compile("\\d");  //d means digits.
		
		Pattern patternObj = Pattern.compile("\\D"); 
		
		Matcher matcherObj = patternObj.matcher(password);
		int count = 0;
		while (matcherObj.find()) {
			count++;
			System.out.print(matcherObj.group());
		}
		System.out.println(count);
	}
}
