package learn_regex;

import java.util.regex.*;

public class Regex_demo4 {
	public static void main(String[] args) {
		String mobile = "8884012810";
		Pattern patternObj = Pattern.compile("[6-9][0-9]{9}");
		Matcher matcherObj = patternObj.matcher(mobile);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}
	}
}
