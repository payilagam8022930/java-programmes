package learn_regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex_demo2 {
	public static void main(String[] args) {
		String password = "Chennai is the capital of TamilNadu";

		// Pattern patternObj = Pattern.compile("TamilNadu$");

		Pattern patternObj = Pattern.compile("^Chennai");
		Matcher matcherObj = patternObj.matcher(password);
		while (matcherObj.find()) {
			System.out.print(matcherObj.group());
		}

	}
}
