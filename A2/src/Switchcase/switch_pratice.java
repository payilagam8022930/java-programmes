package Switchcase;

public class switch_pratice {

	public static void main(String[] args) {
		switch_pratice sp=new switch_pratice();
		char no='a';
		switch(no) {
		case 'x':
			System.out.println("hi");
		case 'a':
			System.out.println("hello");
		default:
			System.out.println("default");
		}
		
		String no1="vicky";
		switch(no1) {
		case "vicky":
			System.out.println("hi");
		case "dhanush":
			System.out.println("hello");
		default:
			System.out.println("default");
		}
		
		Short no3=3;
		switch(no3) {
		case 3:
			System.out.println("hi");
		case 4:
			System.out.println("hello");
		default:
			System.out.println("default");
		}
		
		char no4='a';
		switch(no4) {
		case 'x':
			System.out.println("hi");
		default:
			System.out.println("default");
		case 'a':
			System.out.println("hello");
		
		}
		
    //without default
		int no5=3;
		switch(no5) {
		case 3:
			System.out.println("hi");
		case 4:
			System.out.println("hello");
		}
		
	
	}

}
