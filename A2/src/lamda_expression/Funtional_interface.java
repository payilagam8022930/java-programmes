package lamda_expression;

@FunctionalInterface

public interface Funtional_interface {
	public static void main(String[] args) {
		Funtional_interface.add1();
		}
	
      public int num(int no1,int no2);                   //abstract method
      
      static void add1() {                //static method
    	  System.out.println("add1");
      }
      static void add2() {
    	  System.out.println("add2");
      }
      
      default void add3() {               //default method
    	  System.out.println("add3");
      }
      default void add4() {
    	  System.out.println("add4");
      }
}
