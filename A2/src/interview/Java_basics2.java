package interview;

public class Java_basics2 {

	static String name = "ChatGPT";

	// Function to print the name
	public void name() {
		// Local variable with the same name as the global variable
		String name = "LocalName";

		System.out.println("Local variable: " + name);
		System.out.println("Global variable: " + Java_basics2.name);
	}

	public static void main(String[] args) {
		Java_basics2 jb2 = new Java_basics2();
		System.out.println("Global variable: " + name);
		jb2.name(); // Call the function
	}
}
