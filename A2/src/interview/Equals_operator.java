package interview;

public class Equals_operator {

	public static void main(String[] args) {
		String s1 = "HELLO";
		String s2 = "HELLO";

		System.out.println(s1 == s2);
        System.out.println(s1!=s2);
        
        // check equal or not equal
        int no=10;
        int no1=20;
        int no2=10;
        System.out.println(no==no1);
        System.out.println(no==no2);
	}

}
