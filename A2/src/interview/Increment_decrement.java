package interview;

public class Increment_decrement {

	public static void main(String[] args) {
		//post increment
		int a=10;
		System.out.println(a++);
		System.out.println(a);
		
		//post decrement
		int c=10;
		System.out.println(c--);
		System.out.println(c);
		
		//pre increment
		int b=10;
		System.out.println(++b);
		System.out.println(b);
		
		//pre decrement
		int d=10;
		System.out.println(d--);
		System.out.println(d);

	}

}
