package collections;

public class Comparabledemo implements Comparable<Comparabledemo> {
	int price, gb, mp;

	public Comparabledemo(int i, int j, int k) {
		this.price = i;
		this.gb = j;
		this.mp = k;
	}

	public static void main(String[] args) {
		Comparabledemo samsung = new Comparabledemo(20000, 28, 10);
		Comparabledemo vivo = new Comparabledemo(40000, 64, 9);
		int result = samsung.compareTo(vivo);// samsung->caller object // vivo->called object
		// System.out.println(result);
		if (result > 0) {
			System.out.println("s1 price high");
		} else if (result < 0) {
			System.out.println("s2 price high");
		} else
			System.out.println("s1,s2 equal");
	}

	@Override
	public int compareTo(Comparabledemo vivo) {
		if (this.price > vivo.price) {
					return 0;
		} else if (this.price < vivo.price) {
			return -0;
		}
		return 0;
	}
	
}
