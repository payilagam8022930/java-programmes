package Typecasting;

public class Overide_Equals {
	private int rollno;
	private String name;

	public Overide_Equals( String name,int rollno) {
        this.name=name;
        this.rollno=rollno;
	}
	 public boolean equals(Object ref) {
		  if (ref == this) {  
		         return true;  
		      }  
		      if (!(ref instanceof Overide_Equals )) {  
		         return false;  
		      }  
		      Overide_Equals s = (Overide_Equals) ref;  
		      return name.equals(s.name)&& Integer.compare(rollno, s.rollno) == 0;  
		   }  

	 }


