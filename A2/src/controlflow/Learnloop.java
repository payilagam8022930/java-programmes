package controlflow;

public class Learnloop {

	public static void main(String[] args) {
		Learnloop lp = new Learnloop();
//		lp.loop1();
//		lp.loop2();
//		lp.loop3();
//		lp.loop4();
//		lp.loop5();
		//lp.loop6();
		//lp.loop7();
		//lp.loop8();
		lp.loop9();

	}

	private void loop9() {
		String name ="water";
		for(int i = 0;i<name.length();i++) {
			for(int j=0;j<name.length();j++) {
				if(i==j) {
					System.out.print(name.charAt(j)+" ");
				}
				else 
				{
					System.out.print("  ");
				}
for(int k=5;k<0;k--) {
for(int l=5;l<0;l--) {
	if(k==l) {
		System.out.print(name.charAt(l)+" ");
	}
	else 
	{
		System.out.print("  ");
	}
}
}
			}
			System.out.println();
		}
	}

	private void loop8() {
		int num = 15;
		while (num >=0) {
			System.out.print(num + " ");
			num = num - 3;
		}
	}

	private void loop7() {
		int num = 5;
		while (num >= -1) {
			System.out.print(num + " ");
			num = num - 1;
		}

	}

	private void loop6() {
		int num = 5;
		while (num >=0) {
			System.out.print(num + " ");
			num = num - 1;
		}

	}

	private void loop5() {
		int msg = 1;
		while (msg <= 5) {
			System.out.print("Hi" + " ");
			msg = msg + 1;
		}

	}

	private void loop4() {
		int msg = 0;
		while (msg < 5) {
			System.out.println("hi");
			msg = msg + 1;
		}

	}

	private void loop3() {
		int start = 'a';
		while (start <= 'z') {
			System.out.print((char) start + " ");
			start = start + 1;
		}

	}

	private void loop2() {
		int start = 'a';
		while (start < 'z') {
			System.out.print(start + " ");
			start = start + 1;
		}

	}

	private void loop1() {
		int count = 0;
		while (count < 5) {
			System.out.print(1 + " ");
			count = count + 1;
		}

	}

}
