package controlflow;

public class If_condition {


	public static void main(String[] args) {
		If_condition ex=new If_condition();
		int a=15;
		int b=10;
		int c=10;
		ex.details(a,b,c);
	}

	private void details(int a, int b, int c) {
		if(a>b) {
			if(a>c) {
				System.out.println("A is higher");
			}
			else if(c>a) {
				System.out.println("C is greater");
			}
			else {
				System.out.println("a & c same age");
			}
		}
		else if(b>a){
			if(b>c) {
				System.out.println("B is greater");
			}
			else if(c>b) {
				System.out.println("C is greater");
			}
			else {
				System.out.println("B & C same age");
			}
		}
		else if(a>c) {
			System.out.println("A,B is greater");
		}
		else if(c>a) {
			System.out.println("c is greater");
		}
		else {
			System.out.println("A B C is same age");
		}
		
	}
	

}

