package Programming_class;

public class Sum_of_digit {

	public static void main(String[] args) {
		int no = 9673, total = 0, count = 0;
		while (no > 0) {
			int reminder = no % 10;
			total = total + reminder;
			no = no / 10;
			count = count + 1;
		}
		//total  (9+6+7+3=25)
		System.out.println(total);
		//count
		System.out.println(count);

	}

}
