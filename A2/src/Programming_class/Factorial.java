package Programming_class;

public class Factorial {

	public static void main(String[] args) {
		Factorial fac = new Factorial();

		fac.findfactorial(5);

	}

	private void findfactorial(int no) {
		int fact = 1;
		while (no > 0) {
			fact = fact * no;
			no = no - 1;
		}
		System.out.println(fact);

	}

}
